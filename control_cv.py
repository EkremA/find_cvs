"""
ATTENTION:
This is a control-script should call cv.py for every 
raw bev.82-data-file in the current directory.
Make sure that the directory only has raw data or 
"cvData"-files from previous runs of cv.py or this script.
"""


import numpy as np
import subprocess
import shlex
import datetime as datet

def filecond(fname):
	"""
	Condition for file name in order to separate 
	real raw data from previously generated files in 
	the xray.py- or cv.py- procedures.
	
	Takes a string fname (filename) and returns a boolean value.
	"""
	return fname.startswith("bev.82_") and np.logical_not(fname.endswith("_cvData")) and np.logical_not(fname.endswith("_xrayData")) 


#BENCHMARK-OUTPUT-FILES
#===================================================
byear = datet.datetime.now().year
bmonth = datet.datetime.now().month
bday = datet.datetime.now().day
bhour = datet.datetime.now().hour
bminute = datet.datetime.now().minute
bsecond = datet.datetime.now().second
bmicrosecond = datet.datetime.now().microsecond

benchmarkOutFileName = "cv_benchmark_%s.%s.%s_%s:%s:%s:%s.tc" %(byear, bmonth, bday, bhour, bminute, bsecond, bmicrosecond)

#GET THE FILES
#===================================================
files_list = subprocess.check_output(("ls"))
files_list = files_list.split() #array with all filenames in directory
files_list = np.asarray(files_list, dtype=str) #is numpy-string-array
files_grep = list(map(lambda x: filecond(x), files_list)) #sort out unwanted files
files_grep = np.asarray(files_grep, dtype=bool)
files_grep = files_list[files_grep] 
files_grep = np.asarray(files_grep, dtype=str) #should contain "bev.82_"-data-files

#want to handle the bev-files in a certain order
files_grep_suffix = np.asarray(map(lambda x: x[7::], files_grep), dtype=int)
sortPerm = np.argsort(files_grep_suffix)
files_grep = files_grep[sortPerm]
files_grep_suffix = files_grep_suffix[sortPerm]



#REARRANGE ORDER
#===================================================
#This control-script might take a little long to terminate:
#Decrease resolution to see some outline of cvData early on
#In case of DELTAT=1.0 simulation, I suggest to first have a look 
#at every 5th file then every 2nd and then the rest.

#according masks
files_grep_suffix_5res = files_grep_suffix % 5
files_grep_suffix_5res = np.asarray(map(lambda x: np.allclose(x, 0.0), files_grep_suffix_5res), dtype=bool)
files_grep_suffix_2res = files_grep_suffix % 2
files_grep_suffix_2res = np.asarray(map(lambda x: np.allclose(x, 0.0), files_grep_suffix_2res), dtype=bool)
#get rid of intersections in between the two
files_grep_suffix_2res = np.logical_xor(np.logical_and(files_grep_suffix_2res, files_grep_suffix_5res), files_grep_suffix_2res) #(2res*5res)xor(2res)
files_grep_suffix_rest = np.logical_not(np.logical_or(files_grep_suffix_5res, files_grep_suffix_2res))

#get file-names
files_grep_suffix_5res = files_grep[files_grep_suffix_5res]
files_grep_suffix_2res = files_grep[files_grep_suffix_2res]
files_grep_suffix_rest = files_grep[files_grep_suffix_rest]

#set up new array with resolution-oriented order
files_grep = np.concatenate((files_grep_suffix_5res, files_grep_suffix_2res))
files_grep = np.concatenate((files_grep, files_grep_suffix_rest))



#START LOOP OF COMPUTATIONS
#===================================================
#Keep in mind: benchmark-file is in .tc-file-format
#Header-line for comprehensibility
benchmarkOutFile = open(benchmarkOutFileName, 'a')
header_str = ""
header_str += "Time[NB]    " 		#tNB
header_str += "Time[Myr]    " 		#tMy
header_str += "NPAIRS    " 			#npairs
header_str += "N_WDMS    " 			#nwdms
header_str += "NCVS    " 			#ncvs
header_str += "NDCVS    " 			#ndcvs
header_str += "TOTMASS_WDMS    " 	#np.sum(m_a_wdms)+np.sum(m_b_wdms)
header_str += "TOTMASS_CV    " 		#np.sum(m_a)+np.sum(m_b)
header_str += "TOTMASS_DCV    " 	#np.sum(m_a_d)+np.sum(m_b_d)
header_str += "MAIN    " 			#elapsed_time_main
header_str += "LOAD    " 			#elapsed_time_load
header_str += "COMP    " 			#elapsed_time_comp
header_str += "S1A_WD    " 			#elapsed_time_step1a_wd
header_str += "S1A_APPWD    " 		#elapsed_time_step1a_applywd
header_str += "S1A_NSBHMR    " 		#elapsed_time_step1a_nsbhmr
header_str += "S1A_APPNSBHMR    " 	#elapsed_time_step1a_applynsbhmr
header_str += "S1B_MASS    " 		#elapsed_time_step1b_mass
header_str += "S1B_APPMASS    "		#elapsed_time_step1b_applymass
header_str += "S2    " 				#elapsed_time_step2
header_str += "APPCV    " 			#elapsed_time_applycv
header_str += "FINOUT    " 			#elapsed_time_finaloutput
header_str += "\n"
benchmarkOutFile.write(header_str)
benchmarkOutFile.close()
print("Benchmark-file " + benchmarkOutFileName + " updated!")
#Actual body of file with data and benchmarks
for i in range(0, len(files_grep)):
	#open benchmark file
	benchmarkOutFile = open(benchmarkOutFileName, 'a')
	
	#Start cv.py
	filename = files_grep[i]
	command = "python cv.py " + filename
	print(command)
	command = shlex.split(command)
	bench_line = subprocess.check_output(command)
	benchmarkOutFile.write(bench_line)
	benchmarkOutFile.close()
	print("Benchmark-file " + benchmarkOutFileName + " updated!")
