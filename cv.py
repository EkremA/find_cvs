"""
GOAL: FIND CURRENT CV-CANDIDATES

1. FIND NONCOMPACT*-WD-CV-CANDIDATES
	a. Find MS-WD or evolved-MS-WD-binaries
	b. Check that WD-mass is not smaller than companion-mass (bigger or equal)
2. Check for RLOF

*why "non-compact" and not MS?:
Because evolved-secondary-CVs shall be included too. 
Further differentiation by mass-ratio would follow.

"""
#TODO: Change commenting above and inside code to fit the abbreviation of MS-WD-bin.

import sys
import numpy as np
import time as timemodule
import spy
import rochegeo as rg

start_time_main = timemodule.time()



inFileName = sys.argv[1] #bev.82-file to be read
outFileName = inFileName + "_cvData"



#VARIABLE DECLARATIONS
npairs = 0.0
tMy = 0.0
tNB = np.array([])
i_a = np.array([])
i_b = np.array([])
name_a = np.array([])
name_b = np.array([])
k_a = np.array([])
k_b = np.array([])
k_ab = np.array([])
ri = np.array([])
ecc = np.array([])
per = np.array([])
semi = np.array([])
m_a = np.array([])
m_b = np.array([])
l_a = np.array([])
l_b = np.array([])
rs_a = np.array([])
rs_b = np.array([])
t_a = np.array([])
t_b = np.array([])



#LOAD DATA
#=========================================
start_time_load = timemodule.time()
#first line
temp_str= ""
with open(inFileName, 'r') as openedFile:
	temp_str = openedFile.readline()
temp_str = temp_str.split()
temp_str = map(float, temp_str)
npairs = temp_str[0]
tMy = temp_str[1]
#all other lines
tNB, i_a, i_b, name_a, name_b, k_a, k_b, k_ab, ri, ecc, per, semi, m_a, m_b, l_a, l_b, rs_a, rs_b, t_a, t_b = np.loadtxt(inFileName, skiprows=1, unpack=True)
elapsed_time_load = timemodule.time() - start_time_load


start_time_comp = timemodule.time()

#Step 1a
#=========================================

#check for WDs only at first
#first star in binary
start_time_step1a_wd = timemodule.time()
hewd_a = spy.getMaskEq(k_a, 10.0)
cowd_a = spy.getMaskEq(k_a, 11.0)
onewd_a = spy.getMaskEq(k_a, 12.0)
wd_a = np.logical_or(hewd_a, cowd_a)
wd_a = np.logical_or(wd_a, onewd_a)
#second star in binary
hewd_b = spy.getMaskEq(k_b, 10.0)
cowd_b = spy.getMaskEq(k_b, 11.0)
onewd_b = spy.getMaskEq(k_b, 12.0)
wd_b = np.logical_or(hewd_b, cowd_b)
wd_b = np.logical_or(wd_b, onewd_b)

#mash together: binaries with EXACTLY one WD
wd_mask = np.logical_xor(wd_a, wd_b)
elapsed_time_step1a_wd = timemodule.time() - start_time_step1a_wd
#nsinglewd = len(k_a[wd_mask]) #TODO: Check this line's impact on benchmark
#TODO: Concerning number of memory accesses: limitation is not straightforwardly establishable because you need to read out the values all the time and can't only work with masks if you do this step by step procedure, ACTUALLY NOT FULLY TRUE: YOU CAN STILL MINIMIZE IT...
#TODO: Maybe output benchmarks as command line and start a super-procedure that calls this script and >> redirects the benchmarks maybe as lines (adapt output accordingly) into a benchmark-file
#TODO: More benchmarks maybe, also maybe include number of operations or objects or file sizes or w/e
#TODO: Maximal amount of memory that was used (at the end, cause I do not destruct BUT what about local memory copies in methods?)

#apply mask to reduce CV-candidates-list for efficient continuation
start_time_step1a_applywd = timemodule.time()
tNB = tNB[wd_mask]
i_a = i_a[wd_mask]
i_b = i_b[wd_mask]
name_a = name_a[wd_mask]
name_b = name_b[wd_mask]
k_a = k_a[wd_mask]
k_b = k_b[wd_mask]
k_ab = k_ab[wd_mask]
ri = ri[wd_mask]
ecc = ecc[wd_mask]
per = per[wd_mask]
semi = semi[wd_mask]
m_a = m_a[wd_mask]
m_b = m_b[wd_mask]
l_a = l_a[wd_mask]
l_b = l_b[wd_mask]
rs_a = rs_a[wd_mask]
rs_b = rs_b[wd_mask]
t_a = t_a[wd_mask]
t_b = t_b[wd_mask]
elapsed_time_step1a_applywd = timemodule.time() - start_time_step1a_applywd


#check for companion that is NOT NS, BH or MR
#first star in bin.
start_time_step1a_nsbhmr = timemodule.time()
ns_a = spy.getMaskEq(k_a, 13.0)
bh_a = spy.getMaskEq(k_a, 14.0)
mr_a = spy.getMaskEq(k_a, 15.0)
nsbhmr_a = np.logical_or(ns_a, bh_a)
nsbhmr_a = np.logical_or(nsbhmr_a, mr_a) #True where there is exotica besides WDs


#second star in bin.
ns_b = spy.getMaskEq(k_b, 13.0)
bh_b = spy.getMaskEq(k_b, 14.0)
mr_b = spy.getMaskEq(k_b, 15.0)
nsbhmr_b = np.logical_or(ns_b, bh_b)
nsbhmr_b = np.logical_or(nsbhmr_b, mr_b) #True where there is exotica besides WDs

#mash together
wdevms_mask = np.logical_or(nsbhmr_a, nsbhmr_b)#True where there is at least one NS/BH/MR

#inversing NS/BH/MR-mask to prepare exclusion of such objects
wdevms_mask = np.logical_not(wdevms_mask) #True where there is no (!) NS/BH or MR
elapsed_time_step1a_nsbhmr = timemodule.time() - start_time_step1a_nsbhmr

#We know: only binaries with EXACTLY one WD left.
#So applying wdevms_mask to data will yield NONCOMPACT-WD-binaries
start_time_step1a_applynsbhmr = timemodule.time()
tNB = tNB[wdevms_mask]
i_a = i_a[wdevms_mask]
i_b = i_b[wdevms_mask]
name_a = name_a[wdevms_mask]
name_b = name_b[wdevms_mask]
k_a = k_a[wdevms_mask]
k_b = k_b[wdevms_mask]
k_ab = k_ab[wdevms_mask]
ri = ri[wdevms_mask]
ecc = ecc[wdevms_mask]
per = per[wdevms_mask]
semi = semi[wdevms_mask]
m_a = m_a[wdevms_mask]
m_b = m_b[wdevms_mask]
l_a = l_a[wdevms_mask]
l_b = l_b[wdevms_mask]
rs_a = rs_a[wdevms_mask]
rs_b = rs_b[wdevms_mask]
t_a = t_a[wdevms_mask]
t_b = t_b[wdevms_mask]

#store first relevant findings
#general WD-MS(NONCOMPACT) binaries
tNB_wdms = np.copy(tNB)
i_a_wdms = np.copy(i_a)
i_b_wdms = np.copy(i_b)
name_a_wdms = np.copy(name_a)
name_b_wdms = np.copy(name_b)
k_a_wdms = np.copy(k_a)
k_b_wdms = np.copy(k_b)
k_ab_wdms = np.copy(k_ab)
ri_wdms = np.copy(ri)
ecc_wdms = np.copy(ecc)
per_wdms = np.copy(per)
semi_wdms = np.copy(semi)
m_a_wdms = np.copy(m_a)
m_b_wdms = np.copy(m_b)
l_a_wdms = np.copy(l_a)
l_b_wdms = np.copy(l_b)
rs_a_wdms = np.copy(rs_a)
rs_b_wdms = np.copy(rs_b)
t_a_wdms = np.copy(t_a)
t_b_wdms = np.copy(t_b)

elapsed_time_step1a_applynsbhmr = timemodule.time() - start_time_step1a_applynsbhmr

#Step 1b
#=========================================
#there are only NONCOMPACT-WD-binaries left
#do not know which of the two objects is WD in changed arrays
#trick: browse one of the objects' array for WD: True=this star WD; False=other star WD


#case: first star "a" is WD
start_time_step1b_mass = timemodule.time()
hewd_a = spy.getMaskEq(k_a, 10.0)
cowd_a = spy.getMaskEq(k_a, 11.0)
onewd_a = spy.getMaskEq(k_a, 12.0)
wd_a = np.logical_or(hewd_a, cowd_a)
wd_a = np.logical_or(wd_a, onewd_a) #True: a is WD; False: b is WD

#WD-mass >= NONCOMPACT-mass ?
#wd_a is True where a is WD, hence where b is NONCOMPACT
mass_mask_a = m_a >= m_b
mass_mask_a = np.logical_and(mass_mask_a, wd_a)
#case: second star "b" is WD
#inverse of wd_a is True where b is WD and a is NONCOMPACT
mass_mask_b = m_b >= m_a
mass_mask_b = np.logical_and(mass_mask_b, np.logical_not(wd_a))

#TODO: mass_mask same shape/len check?
#mash together
mass_mask = np.logical_or(mass_mask_a, mass_mask_b) #True if WD-mass >= NONCOMPACT-mass

elapsed_time_step1b_mass = timemodule.time() - start_time_step1b_mass

#apply mask
start_time_step1b_applymass = timemodule.time()
tNB = tNB[mass_mask]
i_a = i_a[mass_mask]
i_b = i_b[mass_mask]
name_a = name_a[mass_mask]
name_b = name_b[mass_mask]
k_a = k_a[mass_mask]
k_b = k_b[mass_mask]
k_ab = k_ab[mass_mask]
ri = ri[mass_mask]
ecc = ecc[mass_mask]
per = per[mass_mask]
semi = semi[mass_mask]
m_a = m_a[mass_mask]
m_b = m_b[mass_mask]
l_a = l_a[mass_mask]
l_b = l_b[mass_mask]
rs_a = rs_a[mass_mask]
rs_b = rs_b[mass_mask]
t_a = t_a[mass_mask]
t_b = t_b[mass_mask]
elapsed_time_step1b_applymass = timemodule.time() - start_time_step1b_applymass

#TODO: uniform mem. access in aplly does not yield any issues

#Step 2
#=========================================
#only objects where the WD is more than or as massive as its companion are left
#find WDs analogous to step 1b
start_time_step2 = timemodule.time()
hewd_a = spy.getMaskEq(k_a, 10.0)
cowd_a = spy.getMaskEq(k_a, 11.0)
onewd_a = spy.getMaskEq(k_a, 12.0)
wd_a = np.logical_or(hewd_a, cowd_a)
wd_a = np.logical_or(wd_a, onewd_a) #True: a is WD; False: b is WD

#TODO: case a and case b not the same shapes
#TODO: doing full blown checks to get masks ---> should be worked out differently
#TODO: write rochegeo.py so that it computes q always in the way that Zaehler is smaller than Nenner

#case a: a is WD: check whether companion has RLOF
#compute Roche Lobe radius
rlr_casea = np.log10(rg.effRLR_Eggleton(m_a, m_b)) + np.log10(1. - ecc) + semi #periastron
#check for RLOF of companion in this case
rlof_casea = rs_b >  rlr_casea
#get the mashed-together mask where a actually is the WD and RLOF of companion happens
rlof_casea = np.logical_and(rlof_casea, wd_a)


#case b: b is WD: check whether companion has RLOF
#compute Roche Lobe radius
rlr_caseb = np.log10(rg.effRLR_Eggleton(m_b, m_a)) + np.log10(1. - ecc) + semi #periastron
#check for RLOF of companion in this case
rlof_caseb = rs_a > rlr_caseb
#get the mashed-together mask where b actually is the WD and RLOF of companion happens
rlof_caseb = np.logical_and(rlof_caseb, np.logical_not(wd_a))

#mash together rlof-arrays of both cases to get final CV-mask
cv_mask = np.logical_or(rlof_casea, rlof_caseb) #True = functional CV ("CV"); False = detached CV ("DCV")
elapsed_time_step2 = timemodule.time() - start_time_step2


#Finally apply filter for CVs
#Get detached CVs (DCVs)
start_time_dcv = timemodule.time()
dcv_mask = np.logical_not(cv_mask)

tNB_d = tNB[dcv_mask]
i_a_d = i_a[dcv_mask]
i_b_d = i_b[dcv_mask]
name_a_d = name_a[dcv_mask]
name_b_d = name_b[dcv_mask]
k_a_d = k_a[dcv_mask]
k_b_d = k_b[dcv_mask]
k_ab_d = k_ab[dcv_mask]
ri_d = ri[dcv_mask]
ecc_d = ecc[dcv_mask]
per_d = per[dcv_mask]
semi_d = semi[dcv_mask]
m_a_d = m_a[dcv_mask]
m_b_d = m_b[dcv_mask]
l_a_d = l_a[dcv_mask]
l_b_d = l_b[dcv_mask]
rs_a_d = rs_a[dcv_mask]
rs_b_d = rs_b[dcv_mask]
t_a_d = t_a[dcv_mask]
t_b_d = t_b[dcv_mask]
elapsed_time_dcv = timemodule.time() - start_time_dcv

#Get functional CVs
start_time_applycv = timemodule.time()
tNB = tNB[cv_mask]
i_a = i_a[cv_mask]
i_b = i_b[cv_mask]
name_a = name_a[cv_mask]
name_b = name_b[cv_mask]
k_a = k_a[cv_mask]
k_b = k_b[cv_mask]
k_ab = k_ab[cv_mask]
ri = ri[cv_mask]
ecc = ecc[cv_mask]
per = per[cv_mask]
semi = semi[cv_mask]
m_a = m_a[cv_mask]
m_b = m_b[cv_mask]
l_a = l_a[cv_mask]
l_b = l_b[cv_mask]
rs_a = rs_a[cv_mask]
rs_b = rs_b[cv_mask]
t_a = t_a[cv_mask]
t_b = t_b[cv_mask]
elapsed_time_applycv = timemodule.time() - start_time_applycv


elapsed_time_comp = timemodule.time() - start_time_comp

#OUTPUT
#=========================================
#Output-file-format same format as bev.82 except:
#Header-1: NPAIRS NumberCVs NumberDCVs Time[Myr]
#NLabel: additional column: CVType("CV"/"DCV")
start_time_finaloutput = timemodule.time()

nwdms = len(k_a_wdms) #number of general WD-MS(NONCOMPACT) binaries
ncvs = len(k_a) #number of CVs
ndcvs = len(k_a_d) #number of detached CVs
with open(outFileName, "w") as outputFile:
	outputFile.write(str(npairs) + "    " + str(ncvs) + "    " + str(ndcvs) + "    " + str(tMy) + "    " + "\n")
	temp_str = ""
	#WD-MS(NONCOMPACT)
	for j in range(nwdms):
		temp_str = "%s   " %(tNB_wdms[j])
		temp_str += "%s   " %(i_a_wdms[j])
		temp_str += "%s   " %(i_b_wdms[j])
		temp_str += "%s   " %(name_a_wdms[j])
		temp_str += "%s   " %(name_b_wdms[j])
		temp_str += "%s   " %(k_a_wdms[j])
		temp_str += "%s   " %(k_b_wdms[j])
		temp_str += "%s   " %(k_ab_wdms[j])
		temp_str += "%s   " %(ri_wdms[j])
		temp_str += "%s   " %(ecc_wdms[j])
		temp_str += "%s   " %(per_wdms[j])
		temp_str += "%s   " %(semi_wdms[j])
		temp_str += "%s   " %(m_a_wdms[j])
		temp_str += "%s   " %(m_b_wdms[j])
		temp_str += "%s   " %(l_a_wdms[j])
		temp_str += "%s   " %(l_b_wdms[j])
		temp_str += "%s   " %(rs_a_wdms[j])
		temp_str += "%s   " %(rs_b_wdms[j])
		temp_str += "%s   " %(t_a_wdms[j])
		temp_str += "%s   " %(t_b_wdms[j])
		temp_str += "WDMS   "
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""
	#CVs
	for j in range(ncvs):
		temp_str = "%s   " %(tNB[j])
		temp_str += "%s   " %(i_a[j])
		temp_str += "%s   " %(i_b[j])
		temp_str += "%s   " %(name_a[j])
		temp_str += "%s   " %(name_b[j])
		temp_str += "%s   " %(k_a[j])
		temp_str += "%s   " %(k_b[j])
		temp_str += "%s   " %(k_ab[j])
		temp_str += "%s   " %(ri[j])
		temp_str += "%s   " %(ecc[j])
		temp_str += "%s   " %(per[j])
		temp_str += "%s   " %(semi[j])
		temp_str += "%s   " %(m_a[j])
		temp_str += "%s   " %(m_b[j])
		temp_str += "%s   " %(l_a[j])
		temp_str += "%s   " %(l_b[j])
		temp_str += "%s   " %(rs_a[j])
		temp_str += "%s   " %(rs_b[j])
		temp_str += "%s   " %(t_a[j])
		temp_str += "%s   " %(t_b[j])
		temp_str += "CV   "
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""
	#DCVs
	for j in range(ndcvs):
		temp_str = "%s   " %(tNB_d[j])
		temp_str += "%s   " %(i_a_d[j])
		temp_str += "%s   " %(i_b_d[j])
		temp_str += "%s   " %(name_a_d[j])
		temp_str += "%s   " %(name_b_d[j])
		temp_str += "%s   " %(k_a_d[j])
		temp_str += "%s   " %(k_b_d[j])
		temp_str += "%s   " %(k_ab_d[j])
		temp_str += "%s   " %(ri_d[j])
		temp_str += "%s   " %(ecc_d[j])
		temp_str += "%s   " %(per_d[j])
		temp_str += "%s   " %(semi_d[j])
		temp_str += "%s   " %(m_a_d[j])
		temp_str += "%s   " %(m_b_d[j])
		temp_str += "%s   " %(l_a_d[j])
		temp_str += "%s   " %(l_b_d[j])
		temp_str += "%s   " %(rs_a_d[j])
		temp_str += "%s   " %(rs_b_d[j])
		temp_str += "%s   " %(t_a_d[j])
		temp_str += "%s   " %(t_b_d[j])
		temp_str += "DCV   "
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""

elapsed_time_finaloutput = timemodule.time() - start_time_finaloutput
elapsed_time_main = timemodule.time() - start_time_main


#BENCHMARK OUTPUT
#=========================================
#Output in .tc-file-format, concretely:
#Time[NB]	Time[Myr]	NPAIRS	N_WD-MS(NONCOMPACT)		NCVs	NDCVs	... additional data/benchmarks ...
if np.allclose(len(tNB), 0.):
	tNB = float(inFileName[7::])	#assume that filename has tNB as suffix
else:
	tNB = tNB[0]	#is the case for len(tNB)>0

bench_str = ""
bench_str += "%s    " 	%(tNB)
bench_str += "%s    " 	%(tMy)
bench_str += "%s    " 	%(npairs)
bench_str += "%s    " 	%(nwdms)
bench_str += "%s    " 	%(ncvs)
bench_str += "%s    " 	%(ndcvs)
bench_str += "%s    " 	%(np.sum(m_a_wdms)+np.sum(m_b_wdms))
bench_str += "%s    " 	%(np.sum(m_a)+np.sum(m_b)) #totmass CVs
bench_str += "%s    " 	%(np.sum(m_a_d)+np.sum(m_b_d))
bench_str += "%s    " 	%(elapsed_time_main)
bench_str += "%s    " 	%(elapsed_time_load)
bench_str += "%s    " 	%(elapsed_time_comp)
bench_str += "%s    " 	%(elapsed_time_step1a_wd)
bench_str += "%s    " 	%(elapsed_time_step1a_applywd)
bench_str += "%s    " 	%(elapsed_time_step1a_nsbhmr)
bench_str += "%s    " 	%(elapsed_time_step1a_applynsbhmr)
bench_str += "%s    " 	%(elapsed_time_step1b_mass)
bench_str += "%s    "	%(elapsed_time_step1b_applymass)
bench_str += "%s    " 	%(elapsed_time_step2)
bench_str += "%s    " 	%(elapsed_time_applycv)
bench_str += "%s    " 	%(elapsed_time_finaloutput)
bench_str += "\n"
print(bench_str)


