"""
Read first lines of sev-/bev/bwdat-files and extract 
- number of objects (N, NPAIRS)
- time in Myr
- time in NB

Plan:	Read first and if needed second line (to get T[NB]) of file,
		copy into an array and finally output into a file.
		
Program relies on proportionality inbetween time[Myr] (or [NB]) and 
input-file-suffix in sorting procedure (see below).

Assuming the files always host at least one object.
"""
import numpy as np
import subprocess
import time as timemodule


def getLinesSevBev(filename):
	"""
	More specifically read the first two lines of sev- or bev-files.
	Filename filename as argument.
	Return time[NB], time[Myr], #objects and needed time as string of given format.
	"""
	temp_str = ""
	timenb = ""
	bench = timemodule.time()
	with open(filename, 'r') as openedFile:
		temp_str = openedFile.readline()
		timenb = openedFile.readline()
	temp_str = temp_str.split()
	timenb = timenb.split()[0] #only the Nbody-time (assuming same for all lines)
	bench = timemodule.time() - bench
	temp_str = "%s		%s		%s		%s		" %(timenb, temp_str[1], temp_str[0], str(bench))
	return temp_str


def getLinesBwdat(filename):
	"""
	More specifically read the first line of bwdat-files.
	Filename filename as argument.
	Return time[NB], time[Myr], #objects and needed time as string of given format.
	"""
	temp_str = ""
	bench = timemodule.time()
	with open(filename, 'r') as openedFile:
		temp_str = openedFile.readline()
	temp_str = temp_str.split() #for uniformal formatting
	bench = timemodule.time() - bench
	temp_str = "%s		%s		%s		%s		" %(temp_str[5], temp_str[6], temp_str[7], str(bench))
	return temp_str


#================================
#FILE MANAGEMENT
#================================
#find the sev-, bev- and bwdat-files in this directory
#filter sev,bev and bwdat
print("Get the list of files in this directory...")
files_list = subprocess.check_output(["ls"])
files_list = files_list.split() #array with all filenames in directory
files_list = np.asarray(files_list, dtype=str) #is numpy-string-array
#TODO: Make check whether there is outfile with same name already there
print("DONE.")
print("Get the sev-, bev- and bwdat-files in this directory...")
files_list = list(filter(lambda x: x[0:3]=="sev" or x[0:3]=="bev" or x[0:5]=="bwdat", files_list)) #only the files starting with "sev"
files_list = np.asarray(files_list, dtype=str)
#separate sev and bev from bwdat
files_mask = np.asarray(list(map(lambda x: x[0:5]=="bwdat", files_list)), dtype=bool)
files_bwdat = files_list[files_mask]
files_list = files_list[np.logical_not(files_mask)] #the rest is sev/bev
#separate sev and bev from one another
files_mask = np.asarray(list(map(lambda x: x[0:3]=="sev", files_list)), dtype=bool)
files_sev = files_list[files_mask]
files_bev = files_list[np.logical_not(files_mask)]
#clean up
files_bwdat = np.asarray(files_bwdat, dtype=str)	#bwdat-files
files_sev = np.asarray(files_sev, dtype=str)		#sev-files
files_bev = np.asarray(files_bev, dtype=str)		#bev-files
#sort them for easier handling later
files_bwdat_ix = np.asarray(list(map(lambda x: x[9::], files_bwdat)), dtype=int) #TODO:Check right conversion
files_bwdat_ix = np.argsort(files_bwdat_ix)
files_bwdat = files_bwdat[files_bwdat_ix]

files_sev_ix = np.asarray(list(map(lambda x: x[7::], files_sev)), dtype=int) #TODO:Check right conversion
files_sev_ix = np.argsort(files_sev_ix)
files_sev = files_sev[files_sev_ix]

files_bev_ix = np.asarray(list(map(lambda x: x[7::], files_bev)), dtype=int) #TODO:Check right conversion
files_bev_ix = np.argsort(files_bev_ix)
files_bev = files_bev[files_bev_ix]
print("DONE.")
#================================
#DATA ANALYSIS
#================================
outSevName = "objectAbundances_Sev.tc" #give this file format the suffix "tc"
outBevName = "objectAbundances_Bev.tc" #give this file format the suffix "tc"
outBwdatName = "objectAbundances_Bwdat.tc" #give this file format the suffix "tc"

print("Start data-readout and -analysis...")


print("Bwdat-files...")
outputBwdat = open(outBwdatName, 'w')
val_bwdat = np.asarray(map(getLinesBwdat, files_bwdat), dtype=str)
val_bwdat = np.asarray(map(lambda x: x + "\n", val_bwdat),dtype=str)
for j in range(0, len(val_bwdat)):
	outputBwdat.write(val_bwdat[j])
outputBwdat.close()
print("DONE.")
print("Sev-files...")
outputSev = open(outSevName, 'w')
val_sev = np.asarray(map(getLinesSevBev, files_sev), dtype=str)
val_sev = np.asarray(map(lambda x: x + "\n", val_sev),dtype=str)
for j in range(0, len(val_sev)):
	outputSev.write(val_sev[j])
outputSev.close()
print("DONE.")
print("Bev-files...")
outputBev = open(outBevName, 'w')
val_bev = np.asarray(map(getLinesSevBev, files_bev), dtype=str)
val_bev = np.asarray(map(lambda x: x + "\n", val_bev),dtype=str)
for j in range(0, len(val_bev)):
	outputBev.write(val_bev[j])
outputBev.close()
print("DONE.")