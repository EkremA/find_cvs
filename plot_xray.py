"""
Plot-routine for findings of xray-binaries 
in control_xray.py-routine.

Loads all *_xrayData-files in current directory.
Attention: Be sure about what files exist in the current directory.
"""
import numpy as np
import matplotlib.pyplot as plt
import sys
import subprocess




def filecond(filen):
	"""
	Returns a boolean values for certain file-names.
	Intended for separating unwanted from wanted files in the current directory.
	Expects that name of file is representative for file's format and content.
	
	Takes string filen (file's name) as argument.
	"""
	return filen.startswith("bev.82_") and filen.endswith("_xrayData")


#GET DATA-FILES
#===================================================
#Get all *_xrayData-files
files_list = subprocess.check_output(("ls"))
files_list = files_list.split() #array with all filenames in directory
files_list = np.asarray(files_list, dtype=str) #is numpy-string-array
files_grep = list(map(lambda x: filecond(x), files_list)) #sort out unwanted files by masking
files_grep = np.asarray(files_grep, dtype=bool)
files_grep = files_list[files_grep] 
files_grep = np.asarray(files_grep, dtype=str) #should contain all xrayData-files

#want to handle the files in a certain order
files_grep_suffix = np.asarray(map(lambda x: x[7:-9:], files_grep), dtype=int) #get the original bev-file-suffices
sortPerm = np.argsort(files_grep_suffix)
files_grep = files_grep[sortPerm] #sorted xrayData-files



#PLOTTING THE DATA
#========================================
#LOAD DATA
#========================================
npairs_arr = np.array([])
nxrays_arr = np.array([])
tMy_arr = np.array([])

tNB_arr = np.array([])
i_a_arr = np.array([])
i_b_arr = np.array([])
name_a_l = []
name_b_l = []
k_a_arr = np.array([])
k_b_arr = np.array([])
k_ab_arr = np.array([])
ri_arr = np.array([])
ecc_arr = np.array([])
per_arr = np.array([])
semi_arr = np.array([])
m_a_arr = np.array([])
m_b_arr = np.array([])
l_a_arr = np.array([])
l_b_arr = np.array([])
rs_a_arr = np.array([])
rs_b_arr = np.array([])
t_a_arr = np.array([])
t_b_arr = np.array([])

for j in range(0, len(files_grep)): #TODO: Set back the right border here len(...)
	inFileName = files_grep[j]
	temp_str= ""
	with open(inFileName, 'r') as openedFile:
		temp_str = openedFile.readline()
	temp_str = temp_str.split()
	print(inFileName, temp_str)
	temp_str = map(float, temp_str)
	npairs_arr = np.append(npairs_arr, temp_str[0])
	nxrays_arr = np.append(nxrays_arr, temp_str[1])
	tMy_arr = np.append(tMy_arr, temp_str[2])
	#all other lines
	if temp_str[1]>0:
		tNB, i_a, i_b, name_a, name_b, k_a, k_b, k_ab, ri, ecc, per, semi, m_a, m_b, l_a, l_b, rs_a, rs_b, t_a, t_b = np.loadtxt(inFileName, skiprows=1, unpack=True)
	#tNB_arr = np.append(tNB_arr, tNB)
	#i_a_arr = np.append(i_a_arr, i_a)
	#i_b_arr = np.append(i_b_arr, i_b)
	name_a = np.asarray(name_a, dtype=str)
	name_b = np.asarray(name_b, dtype=str)
	name_a_l.append(name_a)
	name_b_l.append(name_b)
	#k_a_arr = np.append(k_a_arr, k_a)
	#k_b_arr = np.append(k_b_arr, k_b)
	#k_ab_arr = np.append(k_ab_arr, k_ab)
	#ri_arr = np.append(ri_arr, ri)
	#ecc_arr = np.append(ecc_arr, ecc)
	#per_arr = np.append(per_arr, per)
	#semi_arr = np.append(semi_arr, semi)
	#m_a_arr = np.append(m_a_arr, m_a)
	#m_b_arr = np.append(m_b_arr, m_b)
	#l_a_arr = np.append(l_a_arr, l_a)
	#l_b_arr = np.append(l_b_arr, l_b)
	#rs_a_arr = np.append(rs_a_arr, rs_a)
	#rs_b_arr = np.append(rs_b_arr, rs_b)
	#t_a_arr = np.append(t_a_arr, t_a)
	#t_b_arr = np.append(t_b_arr, t_b)
	
	#TODO: when there are no lines so that there is no error, upper line would be used for name_a-name_b bar diagram, see paper on desk
name_bin_l = np.array([])
for i, name in range(len(name_a_l)):
	name_bin_l = np.append(name_bin_l, 


#ACTUAL PLOT
#========================================
#First panel:
#TMy-NXRAYS
fig, ax1 = plt.subplots()

#TODO:Title for whole figure?
#ax1_style = 'r'
ax1.set_ylabel("Number of xray-binaries", color='r')
ax1.tick_params('y', colors='r')
ax1.set_ylim(bottom=0, top=3*np.amax(nxrays_arr))
#TODO: yticks-->int
ax1.plot(tMy_arr, nxrays_arr, 'r-' ,label="numXRAYS")
#TODO: Maybe just NXRAYS/NCVS/NPAIRS LABELS?
#TMy-NPAIRS
ax2 = ax1.twinx()
ax2.plot(tMy_arr, npairs_arr, '%s-'%('b'), label="numPAIRS")
ax2.tick_params('y', colors='b')
ax2.set_ylim(bottom=0., top=1.3*np.amax(npairs_arr))
ax2.set_ylabel("Number of KS-pairs", color='b')
ax2.grid(True)

#TODO: Latex?
plt.show()