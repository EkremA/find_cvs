#module rochegeo
import numpy as np

def effRLR_Eggleton(m1, m2):
	"""
	Most commonly used formula for effective (!)
	inner Roche Lobe radius, see Eggleton 1983.
	#TODO: Maybe say that separation is normed to unity here??
	m1 and m2 of type float or arrays of floats.
	Expected: m1 >= m2 for every element.
	"""
	#TODO: write rochegeo.py so that it computes q always in the way that Zaehler is smaller than Nenner
	q = m2/m1 #mass-ratio
	q_a = q ** (2./3.)
	q_b = q ** (1./3.)
	return (0.49*q_a)/(0.6*q_a + np.log(1+q_b))