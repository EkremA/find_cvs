#module spy


import numpy as np

#TODO: getMask does ONLY work for expressions, taking ONE arg
def getMask(arr, exprFunc):
	"""
	General purpose single-value-checking mask-generator.
	Get lines with specific values.
	
	A function named exprFunc is applied 
	to each entry in the array arr. 
	At that, exprFunc contains an expression 
	and returns either True or False for given values.
	This function returns a mask-array of same 
	length as arr in the file.
	
	
	Parameters:
	numpy.ndarray arr
	function exprFunc
	
	Returns:
	numpy.ndarray of type boolean
	"""
	return np.asarray(map(exprFunc, arr), dtype=bool)

def getMaskEq(arr, val):
	"""
	Return mask-array where element is True if 
	given array-element from array arr is equal float val.
	"""
	#TODO: Base off of getMask()??
	return np.asarray(map(lambda x: np.allclose(x, val), arr), dtype=bool)



def getMaskWD(arr):
	"""
	Intended for use with NB6++GPU.
	Intended for use with array including stellar-type-number (Hurley et al. 2000).
	
	Return mask-array where element is True if 
	given array-element corresponds to  a white dwarf object.
	"""
	#dummy function