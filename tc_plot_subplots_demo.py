"""
The .tc-file-format ("Type Counter") is a file-format of form:

Time[NB]		Time[Myr]		Quantity 		...

#TODO: Header lines??
One row corresponds to a certain snapshot in time.
The time of the snapshot is given in NB-Units in the first column,
in Myr in the second column and the final counted abundance or 
quantity in the third column. Additional optional information for given problems can 
be added, but the first three columns need to be present in this formatting. 
This is what I rely on with this format.

It is called "Type Counter" because the original idea with this 
format was to store the counted abundance or say quantity of objects 
of a given stellar type at a certain snapshot in time.
Because counting-tasks where output-data needs to be stored
are very common it is practical to decide for a specific formatting
when dealing with these tasks
"""

import matplotlib.pyplot as plt
import pylab as pb
import numpy as np
import sys


def decay(n_beg, lam, t_val ):
	"""
	Zerfalls gesetz
	"""
	return n_beg * np.exp(-1.0*lam*t_val)


arglist = np.asarray(sys.argv[1::], dtype=str) #tc-files

#TODO: CHeck whether workjs fit, check whether same time assumption is ok

time = np.array([]) # assume same times...
quanS = np.array([])
quanB = np.array([])
timeB = np.array([])

time, quanS = np.loadtxt(arglist[0], usecols= (1,2), dtype=float, unpack=True)
timeB, quanB = np.loadtxt(arglist[1], usecols=(1,2), dtype=float, unpack=True)
ratio = 2*quanB/quanS


#plt.subplots_adjust(hspace=0.4)

plt.subplot(221)
plt.plot(time, ratio, "-")
plt.grid(True)
plt.title("linear")
#plt.xlabel("Time [Myr]")

plt.subplot(222)
plt.loglog(time, ratio, "-")
plt.grid(True)
plt.title("log-log")
#plt.xlabel("log(Time [Myr])")

plt.subplot(223)
plt.autoscale(enable=True)
plt.semilogy(time, ratio, "-")
plt.grid(True)
plt.title("semi-log-y")
plt.xlabel("Time [Myr]")

plt.subplot(224)
plt.semilogx(time, ratio, "-")
plt.title("semi-log-x")
plt.grid(True)
plt.xlabel("log(Time [Myr])")


plt.suptitle(r'Abundance-ratio of stars inside of binaries to single stars ($N = 1.1 \times 10^{5}$)')
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

pb.savefig("abundance_ratio_run02072018.pdf", bbox_inches='tight')
plt.show()