"""
GOAL: FIND HMXBs and LMXBs IN KS-BINARIES

PROGRAM-FLOW STEPS:
-----------------------------------------------------------------------
(in the following: sm = solar masses)
1. Find binaries with exactly one NS or BH
2. Find MS*-NS/BH-binaries
3. Distinguish in between LMXB- and HMXB-candidates by checking masses:
	a. find bin. with m_NS/BH >= m_MS ---> LMXB-candidates
	b. find bin. with m_MS >= 10sm ---> HMXB-findings
4. Roche Lobe Overflow (RLOF) check:
	a. LMXB-candidates: if RLOF then "LMXB" else "detached LMXB (DLMXB)"


*over the course of this program, for simplicity reasons,
all noncompact objects (KSTAR<10) will be called "MS", despite
their various evolutionary stages etc.

"""
#TODO: Change commenting above and inside code to fit the abbreviation of MS-WD-bin.

import sys
import numpy as np
import time as timemodule
import spy
import rochegeo as rg

start_time_main = timemodule.time()



inFileName = sys.argv[1] #bev.82-file to be read
outFileName = inFileName + "_xrayData"



#VARIABLE DECLARATIONS
npairs = 0.0
tMy = 0.0
tNB = np.array([])
i_a = np.array([])
i_b = np.array([])
name_a = np.array([])
name_b = np.array([])
k_a = np.array([])
k_b = np.array([])
k_ab = np.array([])
ri = np.array([])
ecc = np.array([])
per = np.array([])
semi = np.array([])
m_a = np.array([])
m_b = np.array([])
l_a = np.array([])
l_b = np.array([])
rs_a = np.array([])
rs_b = np.array([])
t_a = np.array([])
t_b = np.array([])



#LOAD DATA
#=========================================
start_time_load = timemodule.time()
#first line
temp_str= ""
with open(inFileName, 'r') as openedFile:
	temp_str = openedFile.readline()
temp_str = temp_str.split()
temp_str = map(float, temp_str)
npairs = temp_str[0]
tMy = temp_str[1]
#all other lines
tNB, i_a, i_b, name_a, name_b, k_a, k_b, k_ab, ri, ecc, per, semi, m_a, m_b, l_a, l_b, rs_a, rs_b, t_a, t_b = np.loadtxt(inFileName, skiprows=1, unpack=True)
elapsed_time_load = timemodule.time() - start_time_load


start_time_comp = timemodule.time()
#Step 1: Find binaries with exactly one NS or BH
#===============================================
#check for NSs/BHs only at first
#first star in binary
start_time_step1 = timemodule.time()
ns_a = spy.getMaskEq(k_a, 13.0)
bh_a = spy.getMaskEq(k_a, 14.0)
nsbh_a = np.logical_or(ns_a, bh_a)
#second star in binary
ns_b = spy.getMaskEq(k_b, 13.0)
bh_b = spy.getMaskEq(k_b, 14.0)
nsbh_b = np.logical_or(ns_b, bh_b)

#mash together: binaries with EXACTLY one NS or BH
nsbh_mask = np.logical_xor(nsbh_a, nsbh_b)
elapsed_time_step1 = timemodule.time() - start_time_step1
#nsinglensbh = len(k_a[wd_mask]) #TODO: Check this line's impact on benchmark
#TODO: Concerning number of memory accesses: limitation is not straightforwardly establishable because you need to read out the values all the time and can't only work with masks if you do this step by step procedure, ACTUALLY NOT FULLY TRUE: YOU CAN STILL MINIMIZE IT...
#TODO: Maybe output benchmarks as command line and start a super-procedure that calls this script and >> redirects the benchmarks maybe as lines (adapt output accordingly) into a benchmark-file
#TODO: More benchmarks maybe, also maybe include number of operations or objects or file sizes or w/e
#TODO: Maximal amount of memory that was used (at the end, cause I do not destruct BUT what about local memory copies in methods?)
#TODO: X-ray binary version

#apply mask to reduce Xray-Binary-candidates-list for efficient continuation
start_time_step1_apply = timemodule.time()
tNB = tNB[nsbh_mask]
i_a = i_a[nsbh_mask]
i_b = i_b[nsbh_mask]
name_a = name_a[nsbh_mask]
name_b = name_b[nsbh_mask]
k_a = k_a[nsbh_mask]
k_b = k_b[nsbh_mask]
k_ab = k_ab[nsbh_mask]
ri = ri[nsbh_mask]
ecc = ecc[nsbh_mask]
per = per[nsbh_mask]
semi = semi[nsbh_mask]
m_a = m_a[nsbh_mask]
m_b = m_b[nsbh_mask]
l_a = l_a[nsbh_mask]
l_b = l_b[nsbh_mask]
rs_a = rs_a[nsbh_mask]
rs_b = rs_b[nsbh_mask]
t_a = t_a[nsbh_mask]
t_b = t_b[nsbh_mask]
elapsed_time_step1_apply = timemodule.time() - start_time_step1_apply

#Step 2: Find MS*-NS/BH-binaries
#===============================================
#check for companion that is NOT WD or MR
#first star in bin.
start_time_step2 = timemodule.time()
hewd_a = spy.getMaskEq(k_a, 10.0)
cowd_a = spy.getMaskEq(k_a, 11.0)
onewd_a = spy.getMaskEq(k_a, 12.0)
mr_a = spy.getMaskEq(k_a, 15.0)
wdmr_a = np.logical_or(hewd_a, cowd_a)
wdmr_a = np.logical_or(wdmr_a, onewd_a)
wdmr_a = np.logical_or(wdmr_a, mr_a) #True where there are compact objects besides NSs/BHs


#second star in bin.
hewd_b = spy.getMaskEq(k_b, 10.0)
cowd_b = spy.getMaskEq(k_b, 11.0)
onewd_b = spy.getMaskEq(k_b, 12.0)
mr_b = spy.getMaskEq(k_b, 15.0)
wdmr_b = np.logical_or(hewd_b, cowd_b)
wdmr_b = np.logical_or(wdmr_b, onewd_b)
wdmr_b = np.logical_or(wdmr_b, mr_b) #True where there are compact objects besides NSs/BHs

#mash together
wdmr_mask = np.logical_or(wdmr_a, wdmr_b)#True where there is at least one WD or MR

#inversing WD/MR-mask to prepare exclusion of such objects
wdmr_mask = np.logical_not(wdmr_mask) #True where there is no (!) WD or MR
elapsed_time_step2 = timemodule.time() - start_time_step2

#We know: only binaries with EXACTLY one NS/BH left.
#So applying wdmr_mask to data now will yield MS-NS/BH-binaries
start_time_step2_apply = timemodule.time()
tNB = tNB[wdmr_mask]
i_a = i_a[wdmr_mask]
i_b = i_b[wdmr_mask]
name_a = name_a[wdmr_mask]
name_b = name_b[wdmr_mask]
k_a = k_a[wdmr_mask]
k_b = k_b[wdmr_mask]
k_ab = k_ab[wdmr_mask]
ri = ri[wdmr_mask]
ecc = ecc[wdmr_mask]
per = per[wdmr_mask]
semi = semi[wdmr_mask]
m_a = m_a[wdmr_mask]
m_b = m_b[wdmr_mask]
l_a = l_a[wdmr_mask]
l_b = l_b[wdmr_mask]
rs_a = rs_a[wdmr_mask]
rs_b = rs_b[wdmr_mask]
t_a = t_a[wdmr_mask]
t_b = t_b[wdmr_mask]
elapsed_time_step2_apply = timemodule.time() - start_time_step2_apply


#Step 3: LMXB-HMXB-distinguishment
#===============================================
#After this step LMXBs and HMXBs will be treated in separate arrays
#there are only MS-NS/BH-binaries left
#do not know which of the two objects is NS/BH
#Trick 1: browse one of the objects' array for NS/BH: True=this star NS/BH; False=other star NS/BH


#Step 3a: LMXB-candidates
#------------------------
#RLOF-stability-criterion: NS/BH-mass >= MS-mass and MS-mass >= 10sm?

#case: first star "a" is NS/BH
start_time_step3a = timemodule.time()
ns_a = spy.getMaskEq(k_a, 13.0)
bh_a = spy.getMaskEq(k_a, 14.0)
nsbh_a = np.logical_or(ns_a, bh_a) #True: a is NS/BH; False: b is NS/BH

#nsbh_a is True where a is NS/BH hence where b is MS
mass_mask_a = np.logical_and(m_a>=m_b, m_b<=1.)
mass_mask_a = np.logical_and(mass_mask_a, nsbh_a)
#case: second star "b" is NS/BH
#inverse of nsbh_a is True where b is NS/BH and a is MS
mass_mask_b = np.logical_and(m_b>=m_a, m_a<=1.)
mass_mask_b = np.logical_and(mass_mask_b, np.logical_not(nsbh_a))

#TODO: INFO: have applied M2<=1.sm

#mash together
lmxb_mask = np.logical_or(mass_mask_a, mass_mask_b) #True if mass_NS/BH >= mass_MS; detached/active LMXB
elapsed_time_step3a = timemodule.time() - start_time_step3a

#apply mask for LMXB-candidates (detached and functional ones)
start_time_step3a_apply = timemodule.time()
tNB_lmxb = tNB[lmxb_mask]
i_a_lmxb = i_a[lmxb_mask]
i_b_lmxb = i_b[lmxb_mask]
name_a_lmxb = name_a[lmxb_mask]
name_b_lmxb = name_b[lmxb_mask]
k_a_lmxb = k_a[lmxb_mask]
k_b_lmxb = k_b[lmxb_mask]
k_ab_lmxb = k_ab[lmxb_mask]
ri_lmxb = ri[lmxb_mask]
ecc_lmxb = ecc[lmxb_mask]
per_lmxb = per[lmxb_mask]
semi_lmxb = semi[lmxb_mask]
m_a_lmxb = m_a[lmxb_mask]
m_b_lmxb = m_b[lmxb_mask]
l_a_lmxb = l_a[lmxb_mask]
l_b_lmxb = l_b[lmxb_mask]
rs_a_lmxb = rs_a[lmxb_mask]
rs_b_lmxb = rs_b[lmxb_mask]
t_a_lmxb = t_a[lmxb_mask]
t_b_lmxb = t_b[lmxb_mask]
elapsed_time_step3a_apply = timemodule.time() - start_time_step3a_apply


#Step 3b: HMXB-candidates
#------------------------
#
#TODO:CONTINUE HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#mask for first reduction
#hmxb_mask = np.logical_not(lmxb_mask)

#TODO: Delete the 30sm border in BA and comments, in case we find then we can think again...
#TODO: say in BA that although van de Heuvel says M2>10sm we want to include as much as possible + Lewin or so.. that's why >=10
#TODO: make comment in BA about how you exclude any SMBH-MS-bin where MS is of M2>=10. but yet M2<M_bh so RLOF or winds??classic HMXB=??

#apply mask for first reduction
#tNB_hmxb = tNB[hmxb_mask]
#i_a_hmxb = i_a[hmxb_mask]
#i_b_hmxb = i_b[hmxb_mask]
#name_a_hmxb = name_a[hmxb_mask]
#name_b_hmxb = name_b[hmxb_mask]
#k_a_hmxb = k_a[hmxb_mask]
#k_b_hmxb = k_b[hmxb_mask]
#k_ab_hmxb = k_ab[hmxb_mask]
#ri_hmxb = ri[hmxb_mask]
#ecc_hmxb = ecc[hmxb_mask]
#per_hmxb = per[hmxb_mask]
#semi_hmxb = semi[hmxb_mask]
#m_a_hmxb = m_a[hmxb_mask]
#m_b_hmxb = m_b[hmxb_mask]
#l_a_hmxb = l_a[hmxb_mask]
#l_b_hmxb = l_b[hmxb_mask]
#rs_a_hmxb = rs_a[hmxb_mask]
#rs_b_hmxb = rs_b[hmxb_mask]
#t_a_hmxb = t_a[hmxb_mask]
#t_b_hmxb = t_b[hmxb_mask]


#find NS/BH again
#Again we can apply trick 1 from the LMXB-section
#case: first star "a" is NS/BH
start_time_step3b = timemodule.time()
ns_a = spy.getMaskEq(k_a, 13.0)
bh_a = spy.getMaskEq(k_a, 14.0)
nsbh_a = np.logical_or(ns_a, bh_a) #True: a is NS/BH; False: b is NS/BH

#Check whether MS-star has mass of: MS-mass >= 10sm
mass_mask_a = m_b>=10.
mass_mask_a = np.logical_and(nsbh_a, mass_mask_a) #reminder: if star is not NS/BH then it is MS

#case: second star "b" is NS/BH
mass_mask_b = m_a>=10.
mass_mask_b = np.logical_and(np.logical_not(nsbh_a), mass_mask_b)

#mash up
hmxb_mask = np.logical_or(mass_mask_a, mass_mask_b)
elapsed_time_step3b = timemodule.time() - start_time_step3b

#apply new mask for HMXB-candidate (both unstable and stable)
start_time_step3b_apply = timemodule.time()
tNB_hmxb = tNB[hmxb_mask]
i_a_hmxb = i_a[hmxb_mask]
i_b_hmxb = i_b[hmxb_mask]
name_a_hmxb = name_a[hmxb_mask]
name_b_hmxb = name_b[hmxb_mask]
k_a_hmxb = k_a[hmxb_mask]
k_b_hmxb = k_b[hmxb_mask]
k_ab_hmxb = k_ab[hmxb_mask]
ri_hmxb = ri[hmxb_mask]
ecc_hmxb = ecc[hmxb_mask]
per_hmxb = per[hmxb_mask]
semi_hmxb = semi[hmxb_mask]
m_a_hmxb = m_a[hmxb_mask]
m_b_hmxb = m_b[hmxb_mask]
l_a_hmxb = l_a[hmxb_mask]
l_b_hmxb = l_b[hmxb_mask]
rs_a_hmxb = rs_a[hmxb_mask]
rs_b_hmxb = rs_b[hmxb_mask]
t_a_hmxb = t_a[hmxb_mask]
t_b_hmxb = t_b[hmxb_mask]
elapsed_time_step3b_apply = timemodule.time() - start_time_step3b_apply


#TODO: uniform mem. access in aplly does not yield any issues

#Step 4: RLOF-checks
#=========================================
#In the following, only accepted RLOF is with MS-companion as donor

#Step 4a: LMXBs
#-------------------------------
#Reminder: only LMXB-candidates with m_MS >= m_NS/BH left
#find NSs/BHs analogous to above
start_time_step4a = timemodule.time()
ns_a = spy.getMaskEq(k_a_lmxb, 13.0)
bh_a = spy.getMaskEq(k_a_lmxb, 14.0)
nsbh_a = np.logical_or(ns_a, bh_a) #True: a is NS/BH; False: b is NS/BH


#TODO: doing full blown checks to get masks ---> should be worked out differently
#TODO: write rochegeo.py so that it computes q always in the way that Zaehler is smaller than Nenner

#case a: a is NS/BH: check whether companion has RLOF
#compute Roche Lobe radius
rlr_casea = np.log10(rg.effRLR_Eggleton(m_a_lmxb, m_b_lmxb)) + np.log10(1. - ecc_lmxb) + semi_lmxb
#check for RLOF of companion in this case
rlof_casea = rs_b_lmxb >  rlr_casea
#get the mashed-together mask where a actually is the NS/BH and RLOF of MS-companion happens
rlof_casea = np.logical_and(rlof_casea, nsbh_a)


#case b: b is NS/BH: check whether companion has RLOF
#compute Roche Lobe radius
rlr_caseb = np.log10(rg.effRLR_Eggleton(m_b_lmxb, m_a_lmxb)) + np.log10(1. - ecc_lmxb) + semi_lmxb
#check for RLOF of companion in this case
rlof_caseb = rs_a_lmxb > rlr_caseb
#get the mashed-together mask where b actually is the NS/BH and RLOF of companion happens
rlof_caseb = np.logical_and(rlof_caseb, np.logical_not(nsbh_a))

#mash together rlof-arrays of both cases to get final LMXB-mask
lmxb_mask = np.logical_or(rlof_casea, rlof_caseb) #True if functional LMXB, False if detached LMXB


elapsed_time_step4a = timemodule.time() - start_time_step4a


#Finally apply to get both detached and functional LMXBs
start_time_step4a_dlmxb = timemodule.time()
dlmxb_mask = np.logical_not(lmxb_mask) #mask for detached LMXBs

tNB_dlmxb = tNB_lmxb[dlmxb_mask]
i_a_dlmxb = i_a_lmxb[dlmxb_mask]
i_b_dlmxb = i_b_lmxb[dlmxb_mask]
name_a_dlmxb = name_a_lmxb[dlmxb_mask]
name_b_dlmxb = name_b_lmxb[dlmxb_mask]
k_a_dlmxb = k_a_lmxb[dlmxb_mask]
k_b_dlmxb = k_b_lmxb[dlmxb_mask]
k_ab_dlmxb = k_ab_lmxb[dlmxb_mask]
ri_dlmxb = ri_lmxb[dlmxb_mask]
ecc_dlmxb = ecc_lmxb[dlmxb_mask]
per_dlmxb = per_lmxb[dlmxb_mask]
semi_dlmxb = semi_lmxb[dlmxb_mask]
m_a_dlmxb = m_a_lmxb[dlmxb_mask]
m_b_dlmxb = m_b_lmxb[dlmxb_mask]
l_a_dlmxb = l_a_lmxb[dlmxb_mask]
l_b_dlmxb = l_b_lmxb[dlmxb_mask]
rs_a_dlmxb = rs_a_lmxb[dlmxb_mask]
rs_b_dlmxb = rs_b_lmxb[dlmxb_mask]
t_a_dlmxb = t_a_lmxb[dlmxb_mask]
t_b_dlmxb = t_b_lmxb[dlmxb_mask]
elapsed_time_step4a_dlmxb = timemodule.time() - start_time_step4a_dlmxb


start_time_step4a_apply = timemodule.time()
tNB_lmxb = tNB_lmxb[lmxb_mask]
i_a_lmxb = i_a_lmxb[lmxb_mask]
i_b_lmxb = i_b_lmxb[lmxb_mask]
name_a_lmxb = name_a_lmxb[lmxb_mask]
name_b_lmxb = name_b_lmxb[lmxb_mask]
k_a_lmxb = k_a_lmxb[lmxb_mask]
k_b_lmxb = k_b_lmxb[lmxb_mask]
k_ab_lmxb = k_ab_lmxb[lmxb_mask]
ri_lmxb = ri_lmxb[lmxb_mask]
ecc_lmxb = ecc_lmxb[lmxb_mask]
per_lmxb = per_lmxb[lmxb_mask]
semi_lmxb = semi_lmxb[lmxb_mask]
m_a_lmxb = m_a_lmxb[lmxb_mask]
m_b_lmxb = m_b_lmxb[lmxb_mask]
l_a_lmxb = l_a_lmxb[lmxb_mask]
l_b_lmxb = l_b_lmxb[lmxb_mask]
rs_a_lmxb = rs_a_lmxb[lmxb_mask]
rs_b_lmxb = rs_b_lmxb[lmxb_mask]
t_a_lmxb = t_a_lmxb[lmxb_mask]
t_b_lmxb = t_b_lmxb[lmxb_mask]
elapsed_time_step4a_apply  = timemodule.time() - start_time_step4a_apply 


elapsed_time_comp = timemodule.time() - start_time_comp


#OUTPUT
#=========================================
#Output-file-format same format as bev.82 except:
#- Header-1: NPAIRS   N_NS/BH-MS-bin.   N_LMXBs   N_DLMXBs   N_HMXBs   Time[Myr]
#- NLabel: additional column: XrayBinaryType("LMXB"/"DLMXB"/"HMXB")
start_time_finaloutput = timemodule.time()

nnsbhms = len(k_a)		 #number of NS/BH-MS-binaries
nlmxbs = len(k_a_lmxb)	 #number of LMXBs
ndlmxbs = len(k_a_dlmxb) #number of detached LMXBs
nhmxbs = len(k_a_hmxb)	 #number of HMXBs
with open(outFileName, "w") as outputFile:
	outputFile.write(str(npairs) + "    " + str(nnsbhms) + "    " + str(nlmxbs) + "    " + str(ndlmxbs) + "    " + str(nhmxbs) + "    " + str(tMy) + "    " + "\n")
	temp_str = ""
	#NS/BH-MS
	for j in range(nnsbhms):
		temp_str = "%s   " %(tNB[j])
		temp_str += "%s   " %(i_a[j])
		temp_str += "%s   " %(i_b[j])
		temp_str += "%s   " %(name_a[j])
		temp_str += "%s   " %(name_b[j])
		temp_str += "%s   " %(k_a[j])
		temp_str += "%s   " %(k_b[j])
		temp_str += "%s   " %(k_ab[j])
		temp_str += "%s   " %(ri[j])
		temp_str += "%s   " %(ecc[j])
		temp_str += "%s   " %(per[j])
		temp_str += "%s   " %(semi[j])
		temp_str += "%s   " %(m_a[j])
		temp_str += "%s   " %(m_b[j])
		temp_str += "%s   " %(l_a[j])
		temp_str += "%s   " %(l_b[j])
		temp_str += "%s   " %(rs_a[j])
		temp_str += "%s   " %(rs_b[j])
		temp_str += "%s   " %(t_a[j])
		temp_str += "%s   " %(t_b[j])
		temp_str += "%s   " %("NSBHMS")
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""
	#LMXBs
	for j in range(nlmxbs):
		temp_str = "%s   " %(tNB_lmxb[j])
		temp_str += "%s   " %(i_a_lmxb[j])
		temp_str += "%s   " %(i_b_lmxb[j])
		temp_str += "%s   " %(name_a_lmxb[j])
		temp_str += "%s   " %(name_b_lmxb[j])
		temp_str += "%s   " %(k_a_lmxb[j])
		temp_str += "%s   " %(k_b_lmxb[j])
		temp_str += "%s   " %(k_ab_lmxb[j])
		temp_str += "%s   " %(ri_lmxb[j])
		temp_str += "%s   " %(ecc_lmxb[j])
		temp_str += "%s   " %(per_lmxb[j])
		temp_str += "%s   " %(semi_lmxb[j])
		temp_str += "%s   " %(m_a_lmxb[j])
		temp_str += "%s   " %(m_b_lmxb[j])
		temp_str += "%s   " %(l_a_lmxb[j])
		temp_str += "%s   " %(l_b_lmxb[j])
		temp_str += "%s   " %(rs_a_lmxb[j])
		temp_str += "%s   " %(rs_b_lmxb[j])
		temp_str += "%s   " %(t_a_lmxb[j])
		temp_str += "%s   " %(t_b_lmxb[j])
		temp_str += "%s   " %("LMXB")
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""
	#DLMXBs
	for j in range(ndlmxbs):
		temp_str = "%s   " %(tNB_dlmxb[j])
		temp_str += "%s   " %(i_a_dlmxb[j])
		temp_str += "%s   " %(i_b_dlmxb[j])
		temp_str += "%s   " %(name_a_dlmxb[j])
		temp_str += "%s   " %(name_b_dlmxb[j])
		temp_str += "%s   " %(k_a_dlmxb[j])
		temp_str += "%s   " %(k_b_dlmxb[j])
		temp_str += "%s   " %(k_ab_dlmxb[j])
		temp_str += "%s   " %(ri_dlmxb[j])
		temp_str += "%s   " %(ecc_dlmxb[j])
		temp_str += "%s   " %(per_dlmxb[j])
		temp_str += "%s   " %(semi_dlmxb[j])
		temp_str += "%s   " %(m_a_dlmxb[j])
		temp_str += "%s   " %(m_b_dlmxb[j])
		temp_str += "%s   " %(l_a_dlmxb[j])
		temp_str += "%s   " %(l_b_dlmxb[j])
		temp_str += "%s   " %(rs_a_dlmxb[j])
		temp_str += "%s   " %(rs_b_dlmxb[j])
		temp_str += "%s   " %(t_a_dlmxb[j])
		temp_str += "%s   " %(t_b_dlmxb[j])
		temp_str += "%s   " %("DLMXB")
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""
	#HMXBs
	for j in range(nhmxbs):
		temp_str = "%s   " %(tNB_hmxb[j])
		temp_str += "%s   " %(i_a_hmxb[j])
		temp_str += "%s   " %(i_b_hmxb[j])
		temp_str += "%s   " %(name_a_hmxb[j])
		temp_str += "%s   " %(name_b_hmxb[j])
		temp_str += "%s   " %(k_a_hmxb[j])
		temp_str += "%s   " %(k_b_hmxb[j])
		temp_str += "%s   " %(k_ab_hmxb[j])
		temp_str += "%s   " %(ri_hmxb[j])
		temp_str += "%s   " %(ecc_hmxb[j])
		temp_str += "%s   " %(per_hmxb[j])
		temp_str += "%s   " %(semi_hmxb[j])
		temp_str += "%s   " %(m_a_hmxb[j])
		temp_str += "%s   " %(m_b_hmxb[j])
		temp_str += "%s   " %(l_a_hmxb[j])
		temp_str += "%s   " %(l_b_hmxb[j])
		temp_str += "%s   " %(rs_a_hmxb[j])
		temp_str += "%s   " %(rs_b_hmxb[j])
		temp_str += "%s   " %(t_a_hmxb[j])
		temp_str += "%s   " %(t_b_hmxb[j])
		temp_str += "%s   " %("HMXB")
		temp_str += "\n"
		outputFile.write(temp_str)
		temp_str = ""
elapsed_time_finaloutput = timemodule.time() - start_time_finaloutput
elapsed_time_main = timemodule.time() - start_time_main


#BENCHMARK OUTPUT
#=========================================
#Output in .tc-file-format, concretely:
#Time[NB]		Time[Myr]		NPAIRS		N_NS/BH-MS-bin.		N_LMXBs		N_DLMXBs		N_HMXBs		... additional data/benchmarks ...
#TODO: LMXBS, DLMXBS, HMXBS
if np.allclose(len(tNB), 0.):
	tNB = float(inFileName[7::])	#assume that filename has tNB as suffix
else:
	tNB = tNB[0]	#is the case for len(tNB)>0
	
bench_str = ""
bench_str += "%s    " 	%(tNB)
bench_str += "%s    " 	%(tMy)
bench_str += "%s    " 	%(npairs)
bench_str += "%s    " 	%(nnsbhms)
bench_str += "%s    " 	%(nlmxbs)
bench_str += "%s    " 	%(ndlmxbs)
bench_str += "%s    " 	%(nhmxbs)
bench_str += "%s    " 	%(np.sum(m_a)+np.sum(m_b)) #totmass of NS/BH-MS
bench_str += "%s    " 	%(np.sum(m_a_lmxb)+np.sum(m_b_lmxb))
bench_str += "%s    " 	%(np.sum(m_a_dlmxb)+np.sum(m_b_dlmxb))
bench_str += "%s    " 	%(np.sum(m_a_hmxb)+np.sum(m_b_hmxb))
bench_str += "%s    " 	%(elapsed_time_main)
bench_str += "%s    " 	%(elapsed_time_load)
bench_str += "%s    " 	%(elapsed_time_comp)
bench_str += "%s    " 	%(elapsed_time_step1)
bench_str += "%s    " 	%(elapsed_time_step1_apply)
bench_str += "%s    " 	%(elapsed_time_step2)
bench_str += "%s    " 	%(elapsed_time_step2_apply)
bench_str += "%s    " 	%(elapsed_time_step3a)
bench_str += "%s    " 	%(elapsed_time_step3a_apply)
bench_str += "%s    " 	%(elapsed_time_step3b)
bench_str += "%s    " 	%(elapsed_time_step3b_apply)
bench_str += "%s    " 	%(elapsed_time_step4a)
bench_str += "%s    " 	%(elapsed_time_step4a_dlmxb)
bench_str += "%s    " 	%(elapsed_time_step4a_apply)
bench_str += "%s    " 	%(elapsed_time_finaloutput)
bench_str += "\n"
print(bench_str)
